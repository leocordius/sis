from flask import Flask, render_template, request, session, flash, redirect, url_for
from werkzeug.utils import secure_filename
from flaskext.mysql import MySQL
from flask_mail import Mail, Message
from time import gmtime, strftime
from datetime import date
from datetime import datetime

# set the project root directory as the static folder, you can set others.
app = Flask(__name__, static_url_path='')

# database and mailing configurations settings
mysql = MySQL()
app.config['MYSQL_DATABASE_USER'] = 'leo'
app.config['MYSQL_DATABASE_DB'] = 'sis'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)
app.config['MAIL_SERVER'] = 'smtp.gmail.com'
app.config['MAIL_PORT'] = 465
app.config['MAIL_USERNAME'] = 'noreplyateamsis@gmail.com'
app.config['MAIL_PASSWORD'] = 'ateamsis'
app.config['MAIL_USE_TLS'] = False
app.config['MAIL_USE_SSL'] = True
mail = Mail(app)

app.secret_key = '\xd9\x90\x05\xaeOIb5\xe6Td&\xf8\xf4\xdf\xa0-\xc3\xdbc\xb6\x99\xc8D'

# basic routes
@app.route('/')
def hello():
    return render_template('hello.html')


@app.route('/forgot')
def forgot_password():
    return render_template('forgot.html')


@app.route('/sendemail', methods=['POST', 'GET'])
def send_email():
    if request.method == 'POST':
        user_email = request.form['email']
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute("SELECT password from `admin` WHERE email='" + user_email + "'")
        data = cursor.fetchone()
        if data is not None:
            message = "Your password is '" + data[0] + \
                      "\nSend time: " + strftime(strftime("%Y-%m-%d %H:%M:%S", gmtime())) + " GMT" + \
                      "\nBrowser: " + request.headers.get('User-Agent') + \
                      "'.\n\n If you don't know what it is, please contact to SiS admin!";
            send_email(user_email, message)
            flash('Your password has been sent to your email!')
            return render_template('login.html')
        else:
            flash('Your email cannot be found!')
    return render_template('forgot.html')


def send_email(email, message):
    msg = Message("SiS Password", sender="A Team | SiS", recipients=[email])
    msg.body = message
    mail.send(msg)


@app.route('/index')
def index():
    return render_template('index.html')


@app.errorhandler(404)
def page_not_found(error):
    return render_template('page_404.html', error=error), 404


@app.errorhandler(403)
def forbidden(error):
    return render_template('page_403.html', error=error), 403


@app.errorhandler(500)
def logic_error_throw(error):
    return render_template('page_500.html', error=error), 500


# login route
@app.route('/login', methods=['POST', 'GET'])
def login():
    if request.method == 'POST':
        user_email = request.form['email']
        user_password = request.form['password']
        conn = mysql.connect()
        cursor = conn.cursor()
        if is_admin_logging_in(user_email):
            cursor.execute("SELECT email FROM `admin` WHERE email=%s and password=%s", (user_email, user_password))
            data = cursor.fetchone()
            if data is not None:
                session['id'] = data[0]
                cursor.execute(
                    "SELECT title, text, register_time, till_time FROM recent_messages WHERE recent_messages.till_time>now()")
                recent_messages = cursor.fetchall()

                return render_template('admin_home.html', data=data, recent_messages=recent_messages)
            flash("Password is not correct!")
        elif is_student_logging_in(user_email):
            cursor.execute("SELECT student_id, first_name, last_name, photo FROM student WHERE email=%s and password=%s",
                           (user_email, user_password))
            data = cursor.fetchone()

            if data is not None:
                session['id'] = data[0]
                session['email'] = user_email
                session['type'] = 's'
                cursor.execute(
                    "SELECT title, text, register_time, till_time FROM recent_messages WHERE recent_messages.till_time>now()")
                recent_messages = cursor.fetchall()
                cursor.execute(
                    "SELECT title, register_time, till_time FROM to_do WHERE to_do.till_time>now() AND to_do.user_id=%s",
                    (session['id']))
                to_do_list = cursor.fetchall()
                return render_template('student_home.html', data=data, recent_messages=recent_messages, to_do_list=to_do_list)
            flash("Password is not correct!")
        elif is_professor_logging_in(user_email):
            cursor.execute("SELECT professor_id, first_name, last_name, photo FROM professor WHERE email=%s and password=%s",
                           (user_email, user_password))
            data = cursor.fetchone()
            if data is not None:
                session['id'] = data[0]
                session['email'] = user_email
                session['type'] = 'p'
                cursor.execute(
                    "SELECT title, text, register_time, till_time FROM recent_messages WHERE recent_messages.till_time>now()")
                recent_messages = cursor.fetchall()
                return render_template('professor_home.html', data=data, recent_messages=recent_messages)
            flash("Password is not correct!")
        else:
            flash('User has not found!')
            return render_template('login.html')
    return render_template('login.html')


def is_admin_logging_in(user_email):
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute("SELECT email FROM `admin` WHERE email=%s", (user_email))
    data = cursor.fetchone()
    if data is not None:
        return True
    else:
        return False

def is_student_logging_in(user_email):
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute("SELECT email FROM student WHERE email=%s", (user_email))
    data = cursor.fetchone()
    if data is not None:
        return True
    else:
        return False

def is_professor_logging_in(user_email):
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute("SELECT email FROM professor WHERE email=%s", (user_email))
    data = cursor.fetchone()
    if data is not None:
        return True
    else:
        return False


# admin routes
@app.route('/admin_home')
def admin_home():
    if 'id' in session:
        cursor = mysql.connect().cursor()
        cursor.execute("SELECT email FROM `admin` WHERE email=%s", (session['id']))
        data = cursor.fetchone()
        cursor.execute("SELECT title, text, register_time, till_time FROM recent_messages WHERE recent_messages.till_time>now()")
        recent_messages = cursor.fetchall()
        if data is not None:
            return render_template('admin_home.html', data=data, recent_messages=recent_messages)
    flash('You need to login!')
    return render_template('login.html')


@app.route('/recent_messages', methods=['POST', 'GET'])
def recent_messages():
    if request.method == 'POST':
        if 'id' in session:
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.execute("SELECT email FROM `admin` WHERE email=%s", (session['id']))
            data = cursor.fetchone()
            if data is not None:
                title = request.form['title']
                text = request.form['text']
                date_time = request.form['date_time']
                cursor.execute("INSERT INTO recent_messages(title, text, register_time, till_time) VALUES(%s, %s, now(), %s)", (title, text, date_time))
                conn.commit()
                cursor.execute(
                    "SELECT title, text, register_time, till_time FROM recent_messages WHERE recent_messages.till_time>now()")
                recent_messages = cursor.fetchall()
                return render_template('admin_home.html', data=data, recent_messages=recent_messages)
        flash('You need to login!')
        return render_template('login.html')



@app.route('/admin_registration', methods=['POST', 'GET'])
def admin_registration():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute("SELECT email FROM `admin` WHERE email=%s", (session['id']))
    data = cursor.fetchone()
    cursor.execute("SELECT department_id, name FROM department")
    department_rows = cursor.fetchall()
    cursor.execute("SELECT professor_id, first_name, last_name FROM professor")
    professor_rows = cursor.fetchall()
    return render_template('admin_registration.html', data=data, department_rows=department_rows, professor_rows=professor_rows)

@app.route('/admin_departments', methods=['POST', 'GET'])
def admin_departments():
    conn = mysql.connect()
    cursor = conn.cursor()
    cursor.execute("SELECT email FROM `admin` WHERE email=%s", (session['id']))
    data = cursor.fetchone()
    return render_template('admin_departments.html', data=data)


@app.route('/registration', methods=['POST', 'GET'])
def registration():
    if request.method == 'POST':
        type = request.form['type']
        first_name = request.form['first_name']
        last_name = request.form['last_name']
        date_of_birth = request.form['date_of_birth']
        phone = request.form['phone']
        department = request.form['department']
        mail_host = request.form['mail_host']
        mail_server = request.form['mail_server']
        mail = mail_host + "@" + mail_server
        address = request.form['address']
        # photo = request.files['photo']
        # photo_filename = 'uploads/'
        # photo_filename += secure_filename(photo.filename)
        # if not photo_filename == 'uploads/':
        #     photo.save('static/' + photo_filename)
        gender = request.form['gender']
        if request.form['status'] == 'on':
            status = 1;
        else:
            status = 0;
        if type == 's':
            year = str(date.today().year)[2:]
            student_id = '2' + year
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.execute("SELECT count(student_id) from student")
            student_id += str(1000 + cursor.fetchone()[0] + 1)[1:]
            adviser = request.form['adviser']
            credit = request.form['credit']
            year = request.form['year']
            cursor.execute("INSERT INTO student VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, null, null, %s, %s, %s, %s, %s, %s)",
                           (student_id, mail, mail, first_name, last_name, date_of_birth, phone, address, status, 'null', gender, credit, year,adviser, department))
            conn.commit()
            flash('Student registered!')
        else:
            year = str(date.today().year)[2:]
            professor_id = '4' + year
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.execute("SELECT count(professor_id) from professor")
            professor_id += str(1000 + cursor.fetchone()[0] + 1)[1:]
            cursor.execute(
                "INSERT INTO professor VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, null, null, %s, %s, %s)",
                (professor_id, mail, mail, first_name, last_name, date_of_birth, phone, address, status, 'null', gender, department))
            conn.commit()
            flash('Professor registered!')
    return admin_registration()


@app.route('/department_registration', methods=['POST', 'GET'])
def department_registration():
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT email FROM `admin` WHERE email=%s", (session['id']))
    data = cursor.fetchone()
    if request.method == 'POST':
        year = str(date.today().year)[2:]
        department_id = '6' + year
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute("SELECT count(department_id) from department")
        department_id += str(1000 + cursor.fetchone()[0] + 1)[1:]
        department_name = request.form['name']
        department_description = request.form['description']
        department_phone = request.form['phone']
        # department_photo = request.files['photo']
        # photo_filename = secure_filename(department_photo.filename)
        # if photo_filename is not "":
        #     department_photo.save('uploads/' + photo_filename)
        sql = "INSERT INTO department VALUES(%s,'%s', '%s', '%s', '%s')" % (
            department_id, department_name, department_description, department_phone, 'null')
        cursor.execute(sql)
        conn.commit()
        flash('Department registered!')
    return render_template('department_registration.html', data=data)


@app.route('/course_register', methods=['POST', 'GET'])
def course_register():
    if 'id' in session:
        conn = mysql.connect()
        cursor = conn.cursor()
        if request.method == 'POST':
            course_id = str(request.form['professor'])
            cursor.execute("SELECT count(course_id) from course")
            course_id += str(1000 + cursor.fetchone()[0] + 1)[1:]
            name = request.form['name']
            department = request.form['department']
            professor = request.form['professor']
            week_day = request.form.getlist('week_day')
            time1_start = request.form['time1_start']
            time1_end = request.form['time1_end']
            time2_start = request.form['time2_start']
            time2_end = request.form['time2_end']
            place = request.form['place']
            credit = request.form['credit']
            max_students = request.form['max_students']
            description = request.form['description']
            day = 1000000
            for days in week_day:
                day += int(days)
            day = str(day)
            day_abc = ""
            if day[1] == '1': day_abc += '월 '
            if day[2] == '1': day_abc += '화 '
            if day[3] == '1': day_abc += '수 '
            if day[4] == '1': day_abc += '목 '
            if day[5] == '1': day_abc += '금 '
            if day[6] == '1': day_abc += '토 '
            if day.count('1') == 3:
                cursor.execute(
                    "INSERT INTO course VALUES (%s, %s, %s, 0, %s, %s, 0, %s, %s, %s, %s, %s, %s, 0, %s, %s, %s)",
                    (course_id, name, description, credit, max_students, place, str(day)[1:], time1_start, time1_end,
                    time2_start, time2_end, department, professor, day_abc))
            else:
                cursor.execute(
                    "INSERT INTO course VALUES (%s, %s, %s, 0, %s, %s, 0, %s, %s, %s, %s, null, null, 0, %s, %s, %s)",
                    (course_id, name, description, credit, max_students, place, str(day)[1:], time1_start, time1_end,
                     department, professor, day_abc))
            conn.commit()
            flash('Registered successfully!')
        cursor.execute("SELECT department_id, name FROM department")
        department_rows = cursor.fetchall()
        cursor.execute("SELECT professor_id, first_name, last_name FROM professor")
        professor_rows = cursor.fetchall()
        cursor.execute("SELECT email FROM `admin` WHERE email=%s", (session['id']))
        data = cursor.fetchone()
        return render_template('course_register.html', data=data, department_rows=department_rows, professor_rows=professor_rows)
    flash('You need to login!')
    return render_template('login.html')


@app.route('/admin_courses', methods=['POST', 'GET'])
def admin_courses():
    if 'id' in session:
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute("SELECT email FROM `admin` WHERE email=%s", (session['id']))
        data = cursor.fetchone()
        cursor.execute("SELECT c.course_id, c.name, d.name,  p.first_name, p.last_name, c.time1_start, c.time1_end, c.time2_start, c.time2_end, c.credit, "
                       "c.max_students, c.mark, c.day_abc FROM course c, department d, professor p "
                       "WHERE c.department=d.department_id and c.professor=p.professor_id")
        course_rows = cursor.fetchall()
        return render_template('admin_courses.html', data=data, course_rows=course_rows)
    flash('You need to login!')
    return render_template('login.html')


@app.route('/admin_course_delete', methods=['POST', 'GET'])
def admin_course_delete():
    con = mysql.connect()
    cursor = con.cursor()
    course_id = request.args.get('course')
    cursor.execute("DELETE FROM course WHERE course_id=%s", (course_id))
    con.commit()
    return admin_courses()


@app.route('/logout')
def logout():
    session.pop('id', None)
    session.pop('type', None)
    return redirect(url_for('login'))


# student routes
@app.route('/student_home')
def student_home():
    if 'id' in session:
        cursor = mysql.connect().cursor()
        cursor.execute("SELECT student_id, first_name, last_name, photo FROM student WHERE student_id=%s", (session['id']))
        data = cursor.fetchone()
        if data is not None:
            cursor.execute(
                "SELECT title, text, register_time, till_time FROM recent_messages WHERE recent_messages.till_time>now()")
            recent_messages = cursor.fetchall()
            cursor.execute(
                "SELECT title, register_time, till_time FROM to_do WHERE to_do.till_time>now() AND to_do.user_id=%s",
                (session['id']))
            to_do_list = cursor.fetchall()
            return render_template('student_home.html', data=data, recent_messages=recent_messages, to_do_list=to_do_list)
    flash('You need to login!')
    return render_template('login.html')


@app.route('/to_do_list', methods=['POST', 'GET'])
def to_do_list():
    if request.method == 'POST':
        if 'id' in session:
            conn = mysql.connect()
            cursor = conn.cursor()
            cursor.execute("SELECT student_id, first_name, last_name, photo FROM student WHERE student_id=%s",
                           (session['id']))
            data = cursor.fetchone()
            if data is not None:
                title = request.form['title']
                date_time = request.form['date_time']
                cursor.execute("INSERT INTO to_do(user_id, title, register_time, till_time) VALUES(%s, %s, now(), %s)", (session['id'], title, date_time))
                conn.commit()
                cursor.execute(
                    "SELECT title, register_time, till_time FROM to_do WHERE to_do.till_time>now() AND to_do.user_id=%s", (session['id']))
                to_do_list = cursor.fetchall()
                return render_template('student_home.html', data=data, to_do_list=to_do_list)
        flash('You need to login!')
        return render_template('login.html')


@app.route('/student_courses')
def student_courses():
    if 'id' in session:
        cursor = mysql.connect().cursor()
        cursor.execute("SELECT student_id, first_name, last_name, photo FROM student WHERE student_id=%s",
                       (session['id']))
        data = cursor.fetchone()
        cursor.execute("SELECT DISTINCT a.applyment_id, c.course_id, c.name, d.name, p.first_name, p.last_name,c.time1_start, c.time1_end, c.time2_start, c.time2_end, c.credit, c.max_students, c.mark, c.day_abc "
                       "FROM applied a, course c, department d, professor p WHERE a.student=%s and c.course_id=a.course and c.department=d.department_id and c.professor=p.professor_id", (session['id']))
        course_rows = cursor.fetchall()
        return render_template('student_courses.html', data=data, course_rows=course_rows)
    flash('You need to login!')
    return render_template('login.html')


@app.route('/student_course_delete', methods=['POST', 'GET'])
def student_course_delete():
    con = mysql.connect()
    cursor = con.cursor()
    applyment_id = request.args.get('course')
    cursor.execute("DELETE FROM applied WHERE applyment_id=%s", (applyment_id))
    con.commit()
    return student_courses()


@app.route('/student_register')
def student_register():
    if 'id' in session:
        cursor = mysql.connect().cursor()
        cursor.execute("SELECT student_id, first_name, last_name, photo FROM student WHERE student_id=%s",
                       (session['id']))
        data = cursor.fetchone()
        cursor.execute(
            "SELECT c.course_id, c.name, d.name, p.first_name, p.last_name, c.time1_start, c.time1_end, c.time2_start, c.time2_end, c.credit, "
            "c.max_students, c.mark, c.day_abc FROM course c, department d, professor p "
            "WHERE c.department=d.department_id and c.professor=p.professor_id ORDER BY d.name")
        course_rows = cursor.fetchall()
        return render_template('student_register.html', data=data, course_rows=course_rows)
    flash('You need to login!')
    return render_template('login.html')


@app.route('/student_course_register')
def student_course_register():
    if 'id' in session:
        student_id = session['id']
        course_id = request.args.get('course')
        conn = mysql.connect()
        cursor = conn.cursor()
        cursor.execute("SELECT applyment_id FROM applied WHERE student=%s and course=%s", (session['id'], course_id))
        is_applied = cursor.fetchone()
        if is_applied is not None:
            flash('Applied course!')
            return student_register()
        cursor.execute("INSERT INTO applied VALUES(%s, null, null, %s, %s)",
                       (str(student_id)+str(course_id), course_id, student_id))
        conn.commit()
        cursor.execute("UPDATE course SET student_count=student_count+1 WHERE course_id=%s",
                       (course_id))
        conn.commit()
        flash('Course Registered Successfully!')
        return student_register()
    flash('You need to login!')
    return render_template('login.html')


@app.route('/student_scores')
def student_scores():
    if 'id' in session:
        cursor = mysql.connect().cursor()
        cursor.execute("SELECT student_id, first_name, last_name, photo FROM student WHERE student_id=%s",
                       (session['id']))
        data = cursor.fetchone()
        cursor.execute("SELECT d.name, c.name, p.first_name, p.last_name, c.year, c.credit, a.grade "
                       "FROM department d, course c, applied a, professor p "
                       "WHERE a.student=%s AND c.department=d.department_id AND a.course=c.course_id AND c.professor=p.professor_id", (session['id']))
        course_rows = cursor.fetchall()
        return render_template('student_scores.html', data=data, course_rows=course_rows)
    flash('You need to login!')
    return render_template('login.html')


# professor routes
@app.route('/professor_home')
def professor_home():
    if 'id' in session:
        cursor = mysql.connect().cursor()
        cursor.execute("SELECT professor_id, first_name, last_name, photo FROM professor WHERE professor_id=%s", (session['id']))
        data = cursor.fetchone()
        if data is not None:
            cursor.execute(
                "SELECT title, text, register_time, till_time FROM recent_messages WHERE recent_messages.till_time>now()")
            recent_messages = cursor.fetchall()
            return render_template('professor_home.html', data=data, recent_messages=recent_messages)
    flash('You need to login!')
    return render_template('login.html')


@app.route('/professor_courses')
def professor_courses():
    if 'id' in session:
        cursor = mysql.connect().cursor()
        cursor.execute("SELECT professor_id, first_name, last_name, photo FROM professor WHERE professor_id=%s",
                   (session['id']))
        data = cursor.fetchone()
        cursor.execute("SELECT c.course_id, c.name, d.name, c.time1_start, c.time1_end, c.time2_start, c.time2_end, c.credit, "
                       "c.max_students, c.mark, c.day_abc FROM course c, department d "
                       "WHERE c.professor=%s and c.department=d.department_id", (session['id']))
        course_rows = cursor.fetchall()
        return render_template('professor_courses.html', data=data, course_rows=course_rows)
    flash('You need to login!')
    return render_template('login.html')


@app.route('/professor_scores')
def professor_scores():
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT professor_id, first_name, last_name, photo FROM professor WHERE professor_id=%s",
                   (session['id']))
    data = cursor.fetchone()
    cursor.execute(
        "SELECT c.course_id, c.name, d.name, c.time1_start, c.time1_end, c.time2_start, c.time2_end, c.year, "
        "c.credit, c.max_students, c.day_abc FROM course c, department d "
        "WHERE c.professor=%s and c.department=d.department_id", (session['id']))
    course_rows = cursor.fetchall()
    return render_template('professor_scores.html', data=data, course_rows=course_rows)

@app.route('/score')
def score():
    cursor = mysql.connect().cursor()
    cursor.execute("SELECT professor_id, first_name, last_name, photo FROM professor WHERE professor_id=%s",
                   (session['id']))
    data = cursor.fetchone()
    course = request.args.get('course')
    cursor.execute("SELECT course_id, name FROM course WHERE course_id=%s", (course))
    course_info = cursor.fetchone()
    cursor.execute("SELECT s.student_id, d.name, s.first_name, s.last_name, s.year, a.grade FROM student s, department d, applied a "
                   "WHERE s.student_id=a.student AND a.course=%s AND s.department=d.department_id", (course_info[0]))
    student_rows = cursor.fetchall()
    return render_template('score.html', data=data, course_info=course_info, student_rows=student_rows)


@app.route('/score_process', methods=['post'])
def score_process():
    conn = mysql.connect()
    cursor = conn.cursor()
    course = request.args.get('course')
    f = request.form
    for key in f.keys():
        for value in f.getlist(key):
            if key.startswith('score'):
                if value == '': value = None
                cursor.execute("UPDATE applied SET grade=%s WHERE course=%s AND student=%s",
                               (value, course, key[5:]))
                conn.commit()
    return score()


@app.route('/student_mail')
def student_mail():
    if 'id' in session:
        cursor = mysql.connect().cursor()
        cursor.execute("SELECT student_id, first_name, last_name, photo FROM student WHERE student_id=%s",
                           (session['id']))
        data = cursor.fetchone()
        if data is not None:
            cursor.execute("SELECT DISTINCT p.professor_id, d.name, p.first_name, p.last_name "
                               "FROM department d, course c, applied a, professor p "
                               "WHERE a.student=%s AND c.department=d.department_id AND a.course=c.course_id AND c.professor=p.professor_id",
                               (session['id']))
            professor_rows = cursor.fetchall()
            cursor.execute("SELECT m.mail_number, d.name, p.last_name, p.first_name, m.title, m.body, m.time "
                           "FROM mailing m, professor p, department d  "
                           "WHERE sender_id=%s AND m.reciver_id=p.professor_id AND d.department_id=p.department AND m.trash=0",
                    (session['id']))
            sent_mail = cursor.fetchall()
            cursor.execute("SELECT m.mail_number, d.name, p.last_name, p.first_name, m.title, m.body, m.time "
                           "FROM mailing m, professor p, department d  "
                           "WHERE sender_id=p.professor_id AND m.reciver_id=%s AND d.department_id=p.department AND m.trash=0",
                    (session['id']))
            received_mail = cursor.fetchall()
            return render_template('student_mail.html', data=data, professor_rows=professor_rows, sent_mail=sent_mail, received_mail=received_mail)
    flash('You need to login!')
    return render_template('login.html')


@app.route('/professor_mail')
def professor_mail():
    if 'id' in session:
        cursor = mysql.connect().cursor()
        cursor.execute("SELECT professor_id, first_name, last_name, photo FROM professor WHERE professor_id=%s",
                           (session['id']))
        data = cursor.fetchone()
        if data is not None:
            cursor.execute("SELECT DISTINCT s.student_id, d.name, s.first_name, s.last_name "
                               "FROM department d, course c, applied a, student s "
                               "WHERE c.professor=%s AND c.department=d.department_id AND a.course=c.course_id AND a.student=s.student_id",
                               (session['id']))
            student_rows = cursor.fetchall()
            cursor.execute("SELECT m.mail_number, d.name, s.last_name, s.first_name, m.title, m.body, m.time "
                           "FROM mailing m, student s, department d  "
                           "WHERE sender_id=%s AND m.reciver_id=s.student_id AND d.department_id=s.department AND m.trash=0",
                    (session['id']))
            sent_mail = cursor.fetchall()
            cursor.execute("SELECT m.mail_number, d.name, s.last_name, s.first_name, m.title, m.body, m.time "
                           "FROM mailing m, student s, department d  "
                           "WHERE sender_id=s.student_id AND m.reciver_id=%s AND d.department_id=s.department AND m.trash=0",
                           (session['id']))
            received_mail = cursor.fetchall()
            return render_template('professor_mail.html', data=data, student_rows=student_rows, sent_mail=sent_mail, received_mail=received_mail)
    flash('You need to login!')
    return render_template('login.html')


@app.route('/mail_send', methods=['POST'])
def mail_send():
    sender_id = session['id']
    if request.method=='POST':
        reciver_id = request.form['reciver']
        title = request.form['title']
        body = request.form['body']
        time = datetime.now()
        con = mysql.connect()
        cursor = con.cursor()
        cursor.execute("INSERT INTO mailing(sender_id, reciver_id, title, body, ip, time, attachment_file, readd, trash) VALUES(%s, %s, %s, %s, null, %s, null, 0, 0)",
                       (str(sender_id), reciver_id, title, body, time))
        con.commit()
    return student_mail()


@app.route('/mail_send_p', methods=['POST'])
def mail_send_p():
    sender_id = session['id']
    if request.method=='POST':
        reciver_id = request.form['reciver']
        title = request.form['title']
        body = request.form['body']
        time = datetime.now()
        con = mysql.connect()
        cursor = con.cursor()
        cursor.execute("INSERT INTO mailing(sender_id, reciver_id, title, body, ip, time, attachment_file, readd, trash) VALUES(%s, %s, %s, %s, null, %s, null, 0, 0)",
                       (str(sender_id), reciver_id, title, body, time))
        con.commit()
    return professor_mail()


@app.route('/delete_mail_p', methods=['POST', 'GET'])
def delete_mail_p():
    con = mysql.connect()
    cursor = con.cursor()
    mail_number = request.args.get('mail')
    cursor.execute("UPDATE mailing SET trash=1 WHERE mail_number=%s", (mail_number))
    con.commit()
    return professor_mail()


@app.route('/delete_mail', methods=['POST', 'GET'])
def delete_mail():
    con = mysql.connect()
    cursor = con.cursor()
    mail_number = request.args.get('mail')
    cursor.execute("UPDATE mailing SET trash=1 WHERE mail_number=%s", (mail_number))
    con.commit()
    return student_mail()


if __name__ == "__main__":
    app.run(debug=True)
