from flask import Flask
from flaskext.mysql import MySQL

mysql = MySQL()
app = Flask(__name__)
app.config['MYSQL_DATABASE_USER'] = 'leo'
app.config['MYSQL_DATABASE_DB'] = 'sis'
app.config['MYSQL_DATABASE_HOST'] = 'localhost'
mysql.init_app(app)


@app.route("/")
def hello():
    cursor = mysql.connect().cursor()
    return "Welcome to Python Flask App!"


if __name__ == "__main__":
    app.run()